import sys
from PyQt5.QtWidgets import QApplication,QMainWindow
import os
from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
from scipy.interpolate import make_interp_spline
from PyQt5.QtWidgets import QDialog,QApplication,QFileDialog,QMessageBox
from PyQt5.QtCore import *
from PyQt5 import QtCore, QtGui, QtWidgets
import pandas as pd
import matplotlib.pyplot as plt
import time
import datetime
from datetime import datetime, timedelta
from pyzbar import pyzbar
import cv2
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene, QGraphicsPixmapItem
from PyQt5 import QtCore, QtGui, QtWidgets
import ftplib
from ctypes import *
from sqlalchemy import false
from sqlalchemy import false
import shutil
from main import Ui_MainWindow
#時間格式
now_output_time = str(datetime.now().strftime('%Y-%m-%d %H-%M-%S'))
com_now_output = str(datetime.now().strftime('%Y-%m-%d'))

#創建資料夾
if not os.path.isdir('./EGGI_COM'):
    os.mkdir('./EGGI_COM')
    os.mkdir('./EGGI_COM/excel')
    os.mkdir('./EGGI_COM/roi')



def scan_qrcode(qrcode):
    data = pyzbar.decode(qrcode)
    return data[0].data.decode('utf-8')

class myFtp:
    ftp = ftplib.FTP()
    ftp.set_pasv(False)

    def __init__(self,host,port=21):
        self.ftp.connect(host,port)
    
    def Login(self,user,passwd):
        self.ftp.login(user,passwd)
        print(self.ftp.welcome)
    def DownLoadFile(self,LocalFile,RemoteFile): #下載指定目錄下的指定檔案
        file_handler = open(LocalFile,'wb')
        print(file_handler)
        # self.ftp.retrbinary("RETR %s" % (RemoteFile),file_handler.write)#接收伺服器上檔案並寫入本地檔案
        self.ftp.retrbinary('RETR ' + RemoteFile,file_handler.write)
        file_handler.close()
        return True

    def DownLoadFileTree(self,LocalDir,RemoteDir): # 下載整個目錄下的檔案
        print("remoteDir:",RemoteDir)
        if not os.path.exists(LocalDir):
            os.makedirs(LocalDir)
            self.ftp.cwd(RemoteDir)
            RemoteNames = self.ftp.nlst()
            print("RemoteNames",RemoteNames)
            for file in RemoteNames:
                Local = os.path.join(LocalDir,file)
                print(self.ftp.nlst(file))
            if file.find(".") == -1:
                if not os.path.exists(Local):
                    os.makedirs(Local)
                    self.DownLoadFileTree(Local,file)
                else:
                    self.DownLoadFile(Local,file)
                    self.ftp.cwd("..")
                    return True

    #從本地上傳檔案到ftp
    def uploadfile(self,remotepath,localpath):
        bufsize = 1024
        fp = open(localpath,'rb')
        ftp.storbinary('STOR ' + remotepath,fp,bufsize)
        ftp.set_debuglevel(0)
        fp.close() 

    def close(self):
        self.ftp.quit()

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, app):
        super(QMainWindow, self).__init__()
        self.app = app
        self.setup_ui()
        self.connect_signals()
        # self.timer_camera = QtCore.QTimer()
        # self.CAM_NUM = 0
        # self.count = 0

        # self.timer = QTimer()
        # self.qrcode_result1 = []
        # self.qrcode_result2 = []
        # self.qrcode_result3 = []
        # self.qrcode_result4 = []
        # self.qrcode_result5 = []
        # self.qrcode_result6 = []
        # self.qrcode_result7 = []
        # self.qrcode_result8 = []
    def browsefile(self):
        #宣告計算變數
        self.CH1_data = []
        self.CH2_data = []
        self.CH3_data = []
        self.CH4_data = []
        self.CH5_data = []
        self.CH6_data = []
        self.CH7_data = []
        self.CH8_data = []
        self.CH_T_On = []
        self.CH_T_Off = []
        self.CH_total = []
        self.slot_high = []
        self.slot_low = []
        self.CH_slot = []
        self.TF_array = []
        #開啟TXT檔案
        self.fname = QFileDialog.getOpenFileName(self, '開啟txt檔案', 'C:\Program Files (x86)', 'txt files (*.txt)') # " C:\python\Learn_Python\Temperature" 是自己的電腦位置路徑
        if(self.fname[0]==""):
            print("No file")
        else:
            if not os.path.isdir('./image'):
                os.mkdir('./image')              
            if not os.path.isdir('./result'):
                os.mkdir('./result')
            if not os.path.isdir('./EGGI_Temperature'):
                os.mkdir('./EGGI_Temperature')
            self.input_file.setText(self.fname[0])
            self.df = pd.read_csv(self.fname[0], delimiter='\t')
            self.df.columns = ['time','index', 'index', 'CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8']  # 在開啟檔案上面新增一行
            print("Open file(.txt) >> " + str(self.fname[0]))
            # X軸
            for i in range(0, len(self.df.index), 1):
                self.CH_total.append(i)
            # Y軸
            for i in range(0, len(self.df.index), 1):
                self.CH1_data.append(self.df.loc[i, 'CH1'])
                self.CH2_data.append(self.df.loc[i, 'CH2'])
                self.CH3_data.append(self.df.loc[i, 'CH3'])
                self.CH4_data.append(self.df.loc[i, 'CH4'])
                self.CH5_data.append(self.df.loc[i, 'CH5'])
                self.CH6_data.append(self.df.loc[i, 'CH6'])
                self.CH7_data.append(self.df.loc[i, 'CH7'])
                self.CH8_data.append(self.df.loc[i, 'CH8'])
            
            for ch in range(1, 9, 1):
                if ch == 1:
                    self.CH_data = [[self.df.loc[i, 'CH' + str(ch)] for i in range(len(self.df.index))]]
                else:
                    self.CH_data.append([self.df.loc[i, 'CH' + str(ch)] for i in range(len(self.df.index))])
            # 存取每個Channel的值到陣列

            for i in range(0, 8, 1):
                self.T_On_array = []
                self.T_Off_array = []
                # 對一個Channel進行資料搜尋
                for ch in range(0, len(self.df.index) - 1, 1):
                    a = self.CH_data[i][ch + 1] - self.CH_data[i][ch]
                    b = self.CH_data[i][ch] - self.CH_data[i][ch-1]
                    # 達成T_On條件把資料存進self.T_On_array陣列
                    if a < 0 and self.CH_data[i][ch] > 109 and b > 0:
                        self.T_On_array.append(self.CH_data[i][ch])
                        self.slot_high.append(ch)
                    # 達成T_Off條件把資料存進self.T_Off_array陣列
                    if a > 0 and 73 < self.CH_data[i][ch] < 74 and b < 0:
                        self.T_Off_array.append(self.CH_data[i][ch])
                        self.slot_low.append(ch)
                # 溫度資料發生其他狀況
                if self.CH_data[i][0] == -204.8 or self.CH_data[i][2] == self.CH_data[i][3]:
                    # 如果溫度未加熱保持恆溫
                    if self.CH_data[i][2] != -204.8:
                        for j in range(1, 9, 1):
                            self.T_On_array.append(self.CH_data[i][2]) #"恆溫"
                            self.T_Off_array.append(self.CH_data[i][2])
                    # 如果沒有連接上
                    for k in range(1, 9, 1):
                        self.T_On_array.append(0)
                        self.T_Off_array.append(0)
                # print(self.T_On_array)
                self.CH_T_On.append(self.T_On_array[0])
                print(self.CH_T_On)
                self.CH_T_Off.append(self.T_Off_array[0])
                value_gap = self.T_On_array[0] - self.T_Off_array[0]
                time_gap = self.slot_high[0] - self.slot_low[0]
                self.CH_slot.append(round(float(value_gap/time_gap),2))
            print("-"*20 + "CH_T_On" + "-"*20) 
            for i in range(0,8,1):
                print(self.CH_T_On[i])
            print("-"*20 + "CH_T_Off" + "-"*20)
            for i in range(0,8,1):
                print(self.CH_T_Off[i])

            # 將On跟Off陣列存取的資料對應至各個位置上
            self.ch1_T_On.setText(str(self.CH_T_On[0]))
            self.ch1_T_Off.setText(str(self.CH_T_Off[0]))
            self.ch2_T_On.setText(str(self.CH_T_On[1]))
            self.ch2_T_Off.setText(str(self.CH_T_Off[1]))
            self.ch3_T_On.setText(str(self.CH_T_On[2]))
            self.ch3_T_Off.setText(str(self.CH_T_Off[2]))
            self.ch4_T_On.setText(str(self.CH_T_On[3]))
            self.ch4_T_Off.setText(str(self.CH_T_Off[3]))
            self.ch5_T_On.setText(str(self.CH_T_On[4]))
            self.ch5_T_Off.setText(str(self.CH_T_Off[4]))
            self.ch6_T_On.setText(str(self.CH_T_On[5]))
            self.ch6_T_Off.setText(str(self.CH_T_Off[5]))
            self.ch7_T_On.setText(str(self.CH_T_On[6]))
            self.ch7_T_Off.setText(str(self.CH_T_Off[6]))
            self.ch8_T_On.setText(str(self.CH_T_On[7]))
            self.ch8_T_Off.setText(str(self.CH_T_Off[7]))
            #####每個Channel的斜率
            self.ch1_slope.setText(str(self.CH_slot[0]))
            self.ch2_slope.setText(str(self.CH_slot[1]))
            self.ch3_slope.setText(str(self.CH_slot[2]))
            self.ch4_slope.setText(str(self.CH_slot[3]))
            self.ch5_slope.setText(str(self.CH_slot[4]))
            self.ch6_slope.setText(str(self.CH_slot[5]))
            self.ch7_slope.setText(str(self.CH_slot[6]))
            self.ch8_slope.setText(str(self.CH_slot[7]))
            #####每個channel結果Pass或Fail
            # 資料對應
            P = "Pass"
            F = "Fail"
            N = "未連接"
            W = "未加熱"
            # 對應顏色
            T_color = "color: green;"
            F_color = "color: gray;"
            constant_color = "color: orange;"
            # 儲存結果

            # CH1PF
            if self.CH_slot[0] == 0 or self.CH_T_On[0] == self.CH_T_Off[0]:
                if self.CH_T_On[0] == self.CH_T_Off[0] != 0:
                    self.ch1_PF.setText(str(W))
                    self.ch1_PF.setStyleSheet(constant_color)
                    self.TF_array.append(str(W))
                else:
                    self.ch1_PF.setText(str(N))
                    self.ch1_PF.setStyleSheet(F_color)
                    self.TF_array.append(str(N))
            else:
                self.ch1_PF.setText(str(P))
                self.ch1_PF.setStyleSheet(T_color)
                self.TF_array.append(str(P))
            # CH2PF
            if self.CH_slot[1] == 0 or self.CH_T_On[1] == self.CH_T_Off[1]:
                if self.CH_T_On[1] == self.CH_T_Off[1] != 0:
                    self.ch2_PF.setText(W)
                    self.ch2_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch2_PF.setText(N)
                    self.ch2_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch2_PF.setText(P)
                self.ch2_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH3PF
            if self.CH_slot[2] == 0 or self.CH_T_On[2] == self.CH_T_Off[2]:
                if self.CH_T_On[2] == self.CH_T_Off[2] != 0:
                    self.ch3_PF.setText(W)
                    self.ch3_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch3_PF.setText(N)
                    self.ch3_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch3_PF.setText(P)
                self.ch3_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH4PF
            if self.CH_slot[3] == 0 or self.CH_T_On[3] == self.CH_T_Off[3]:
                if self.CH_T_On[3] == self.CH_T_Off[3] != 0:
                    self.ch4_PF.setText(W)
                    self.ch4_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch4_PF.setText(N)
                    self.ch4_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch4_PF.setText(P)
                self.ch4_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH5PF
            if self.CH_slot[4] == 0 or self.CH_T_On[4] == self.CH_T_Off[4]:
                if self.CH_T_On[4] == self.CH_T_Off[4] != 0:
                    self.ch5_PF.setText(W)
                    self.ch5_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch5_PF.setText(N)
                    self.ch5_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch5_PF.setText(P)
                self.ch5_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH6PF
            if self.CH_slot[5] == 0 or self.CH_T_On[5] == self.CH_T_Off[5]:
                if self.CH_T_On[5] == self.CH_T_Off[5] != 0:
                    self.ch6_PF.setText(W)
                    self.ch6_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch6_PF.setText(N)
                    self.ch6_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch6_PF.setText(P)
                self.ch6_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH7PF
            if self.CH_slot[6] == 0 or self.CH_T_On[6] == self.CH_T_Off[6]:
                if self.CH_T_On[6] == self.CH_T_Off[6] != 0:
                    self.ch7_PF.setText(W)
                    self.ch7_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch7_PF.setText(N)
                    self.ch7_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch7_PF.setText(P)
                self.ch7_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            # CH8PF
            if self.CH_slot[7] == 0 or self.CH_T_On[7] == self.CH_T_Off[7]:
                if self.CH_T_On[7] == self.CH_T_Off[7] != 0:
                    self.ch8_PF.setText(W)
                    self.ch8_PF.setStyleSheet(constant_color)
                    self.TF_array.append(W)
                else:
                    self.ch8_PF.setText(N)
                    self.ch8_PF.setStyleSheet(F_color)
                    self.TF_array.append(N)
            else:
                self.ch8_PF.setText(P)
                self.ch8_PF.setStyleSheet(T_color)
                self.TF_array.append(P)
            self.take_picture()

            # -----------------------------------------------------------------------
            img = cv2.imread("image/CH1.jpg")
            img2 = cv2.imread("image/CH2.jpg")
            img3 = cv2.imread("image/CH3.jpg")
            img4 = cv2.imread("image/CH4.jpg")
            img5 = cv2.imread("image/CH5.jpg")
            img6 = cv2.imread("image/CH6.jpg")
            img7 = cv2.imread("image/CH7.jpg")
            img8 = cv2.imread("image/CH8.jpg")
            # 轉換影象通道
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
            img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2RGB)
            img4 = cv2.cvtColor(img4, cv2.COLOR_BGR2RGB)
            img5 = cv2.cvtColor(img5, cv2.COLOR_BGR2RGB)
            img6 = cv2.cvtColor(img6, cv2.COLOR_BGR2RGB)
            img7 = cv2.cvtColor(img7, cv2.COLOR_BGR2RGB)
            img8 = cv2.cvtColor(img8, cv2.COLOR_BGR2RGB)
            # 獲取影象大小
            x = img.shape[1]
            y = img.shape[0]
            x2 = img2.shape[1]
            y2 = img2.shape[0]
            x3 = img3.shape[1]
            y3 = img3.shape[0]
            x4 = img4.shape[1]
            y4 = img4.shape[0]
            x5 = img5.shape[1]
            y5 = img5.shape[0]
            x6 = img6.shape[1]
            y6 = img6.shape[0]
            x7 = img7.shape[1]
            y7 = img7.shape[0]
            x8 = img8.shape[1]
            y8 = img8.shape[0]
            # 圖片放縮尺度
            # self.zoomscale = 1
            frame = QImage(img, x, y, x * 3, QImage.Format_RGB888)
            self.pix = QPixmap.fromImage(frame)
            frame2 = QImage(img2, x2, y2, x2 * 3, QImage.Format_RGB888)
            self.pix2 = QPixmap.fromImage(frame2)
            frame3 = QImage(img3, x3, y3, x3 * 3, QImage.Format_RGB888)
            self.pix3 = QPixmap.fromImage(frame3)
            frame4 = QImage(img4, x4, y4, x4 * 3, QImage.Format_RGB888)
            self.pix4 = QPixmap.fromImage(frame4)
            frame5 = QImage(img5, x5, y5, x5 * 3, QImage.Format_RGB888)
            self.pix5 = QPixmap.fromImage(frame5)
            frame6 = QImage(img6, x6, y6, x6 * 3, QImage.Format_RGB888)
            self.pix6 = QPixmap.fromImage(frame6)
            frame7 = QImage(img7, x7, y7, x7 * 3, QImage.Format_RGB888)
            self.pix7 = QPixmap.fromImage(frame7)
            frame8 = QImage(img8, x8, y8, x8 * 3, QImage.Format_RGB888)
            self.pix8 = QPixmap.fromImage(frame8)
            # 建立畫素圖元
            self.item = QGraphicsPixmapItem(self.pix)
            self.item2 = QGraphicsPixmapItem(self.pix2)
            self.item3 = QGraphicsPixmapItem(self.pix3)
            self.item4 = QGraphicsPixmapItem(self.pix4)
            self.item5 = QGraphicsPixmapItem(self.pix5)
            self.item6 = QGraphicsPixmapItem(self.pix6)
            self.item7 = QGraphicsPixmapItem(self.pix7)
            self.item8 = QGraphicsPixmapItem(self.pix8)
            # 建立場景
            self.scene = QGraphicsScene()
            self.scene2 = QGraphicsScene()
            self.scene3 = QGraphicsScene()
            self.scene4 = QGraphicsScene()
            self.scene5 = QGraphicsScene()
            self.scene6 = QGraphicsScene()
            self.scene7 = QGraphicsScene()
            self.scene8 = QGraphicsScene()
            self.scene.addItem(self.item)
            self.scene2.addItem(self.item2)
            self.scene3.addItem(self.item3)
            self.scene4.addItem(self.item4)
            self.scene5.addItem(self.item5)
            self.scene6.addItem(self.item6)
            self.scene7.addItem(self.item7)
            self.scene8.addItem(self.item8)
            # 將場景新增至檢視
            self.ch1_chart.setScene(self.scene)
            self.ch2_chart.setScene(self.scene2)
            self.ch3_chart.setScene(self.scene3)
            self.ch4_chart.setScene(self.scene4)
            self.ch5_chart.setScene(self.scene5)
            self.ch6_chart.setScene(self.scene6)
            self.ch7_chart.setScene(self.scene7)
            self.ch8_chart.setScene(self.scene8)
    def take_picture(self):
        # ---------------CH1---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH1_data, 'o-', color='red', label="CH1_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH1.jpg')
        # ---------------CH2---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH2_data, 'o-', color='orange', label="CH2_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH2.jpg')
        # ---------------CH3---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH3_data, 'o-', color='yellow', label="CH3_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH3.jpg')
        # ---------------CH4---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH4_data, 'o-', color='green', label="CH4_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH4.jpg')
        # ---------------CH5---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH5_data, 'o-', color='#6F00FF', label="CH5_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH5.jpg')
        # ---------------CH6---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH6_data, 'o-', color='m', label="CH6_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH6.jpg')
        # ---------------CH7---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH7_data, 'o-', color='purple', label="CH7_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH7.jpg')
        # ---------------CH8---------------------
        plt.figure(figsize=(3, 3), dpi=60, linewidth=0)
        plt.plot(self.CH_total, self.CH8_data, 'o-', color='k', label="CH8_data")  # 紅
        plt.xlim(0, len(self.df.index))  # 設定圖範圍
        plt.ylim(0, 130)  # 設定圖範圍
        plt.savefig('image/CH8.jpg')
    
    def save_log(self):
        if self.input_name.text() == "":
            QtWidgets.QMessageBox.critical(self, u"存取失敗", u"請輸入操作人員", buttons=QtWidgets.QMessageBox.Ok,
                                          defaultButton=QtWidgets.QMessageBox.Ok)
            print("請輸入操作人員")
        elif self.input_file.text() == "":
            QtWidgets.QMessageBox.warning(self, u"存取失敗", u"未開啟檔案", buttons=QtWidgets.QMessageBox.Ok,
                                          defaultButton=QtWidgets.QMessageBox.Ok)
            print("未開啟檔案")
        elif self.ch1_qrcodelable.text() == "" or self.ch2_qrcodelable.text() == "" or self.ch3_qrcodelable.text() == "" or self.ch4_qrcodelable.text() == "" or self.ch5_qrcodelable.text() == "" or self.ch6_qrcodelable.text() == "" or self.ch7_qrcodelable.text() == "" or self.ch8_qrcodelable.text() == "":
            QtWidgets.QMessageBox.warning(self, u"存取失敗", u"Qrcode未掃", buttons=QtWidgets.QMessageBox.Ok,
                                          defaultButton=QtWidgets.QMessageBox.Ok)
            print("Qrcode未掃")
        else:
            QtWidgets.QMessageBox.information(self, u"存取成功", u"已成功另存Excel檔案", buttons=QtWidgets.QMessageBox.Ok,
                                          defaultButton=QtWidgets.QMessageBox.Ok)

            self.save_excel = pd.DataFrame({"Qrcode": [self.qrcode_result1[0], self.qrcode_result2[0],
                                                       self.qrcode_result3[0], self.qrcode_result4[0],
                                                       self.qrcode_result5[0], self.qrcode_result6[0],
                                                       self.qrcode_result7[0], self.qrcode_result8[0]],
                                            "T_On": [self.CH_T_On[0], self.CH_T_On[1], self.CH_T_On[2], self.CH_T_On[3],
                                                     self.CH_T_On[4], self.CH_T_On[5], self.CH_T_On[6],
                                                     self.CH_T_On[7]],
                                            "T_Off": [self.CH_T_Off[0], self.CH_T_Off[1], self.CH_T_Off[2],
                                                      self.CH_T_Off[3], self.CH_T_Off[4], self.CH_T_Off[5],
                                                      self.CH_T_Off[6], self.CH_T_Off[7]],
                                            "Slope" :[self.CH_slot[0], self.CH_slot[1], self.CH_slot[2],
                                                      self.CH_slot[3],self.CH_slot[4], self.CH_slot[5],
                                                      self.CH_slot[6], self.CH_slot[7]],
                                            "檢測結果": [self.TF_array[0], self.TF_array[1], self.TF_array[2],
                                                     self.TF_array[3],
                                                     self.TF_array[4], self.TF_array[5], self.TF_array[6],
                                                     self.TF_array[7]],
                                            "操作人員": [self.input_name.text(), "", "", "", "", "", "", ""],
                                            "檔案來源": [self.fname[0], "", "", "", "", "", "", ""]
                                            }, index=['CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8'])
            self.save_excel.to_excel('./result/history' + now_output_time+"output.xlsx", encoding="utf_8_sig")
            print("儲存成功")
    def clean_log(self):
        # T_On全關
        self.ch1_T_On.setText("")
        self.ch2_T_On.setText("")
        self.ch3_T_On.setText("")
        self.ch4_T_On.setText("")
        self.ch5_T_On.setText("")
        self.ch6_T_On.setText("")
        self.ch7_T_On.setText("")
        self.ch8_T_On.setText("")
        # T_Off全關
        self.ch1_T_Off.setText("")
        self.ch2_T_Off.setText("")
        self.ch3_T_Off.setText("")
        self.ch4_T_Off.setText("")
        self.ch5_T_Off.setText("")
        self.ch6_T_Off.setText("")
        self.ch7_T_Off.setText("")
        self.ch8_T_Off.setText("")
        # PF全關
        self.ch1_PF.setText("")
        self.ch2_PF.setText("")
        self.ch3_PF.setText("")
        self.ch4_PF.setText("")
        self.ch5_PF.setText("")
        self.ch6_PF.setText("")
        self.ch7_PF.setText("")
        self.ch8_PF.setText("")
        # 欄位
        self.input_file.setText("")
        self.input_name.setText("")
        # Slope
        self.ch1_slope.setText("")
        self.ch2_slope.setText("")
        self.ch3_slope.setText("")
        self.ch4_slope.setText("")
        self.ch5_slope.setText("")
        self.ch6_slope.setText("")
        self.ch7_slope.setText("")
        self.ch8_slope.setText("")
        # #Chart
        self.ch1_chart.setScene(None)
        self.ch2_chart.setScene(None)
        self.ch3_chart.setScene(None)
        self.ch4_chart.setScene(None)
        self.ch5_chart.setScene(None)
        self.ch6_chart.setScene(None)
        self.ch7_chart.setScene(None)
        self.ch8_chart.setScene(None)
    def setup_ui(self):
        self.setupUi(self)
    def connect_signals(self):
        self.btn_clean.clicked.connect(self.clean_log)
        self.btn_opentxt.clicked.connect(self.browsefile)
        self.btn_save.clicked.connect(self.save_log)
        self.ch1_qrcode.clicked.connect(self.qrcode1)
        self.ch2_qrcode.clicked.connect(self.qrcode2)
        self.ch3_qrcode.clicked.connect(self.qrcode3)
        self.ch4_qrcode.clicked.connect(self.qrcode4)
        self.ch5_qrcode.clicked.connect(self.qrcode5)
        self.ch6_qrcode.clicked.connect(self.qrcode6)
        self.ch7_qrcode.clicked.connect(self.qrcode7)
        self.ch8_qrcode.clicked.connect(self.qrcode8)
        self.ch1_display.clicked.connect(self.display1)
        self.ch2_display.clicked.connect(self.display2)
        self.ch3_display.clicked.connect(self.display3)
        self.ch4_display.clicked.connect(self.display4)
        self.ch5_display.clicked.connect(self.display5)
        self.ch6_display.clicked.connect(self.display6)
        self.ch7_display.clicked.connect(self.display7)
        self.ch8_display.clicked.connect(self.display8)
        # self.input_name.text() #輸入操作人員
        # self.btn_opentxt_com.clicked.connect(self.com_csv)
        # self.btn_save_com.clicked.connect(self.com_save)
        # self.btn_clean_com.clicked.connect(self.com_clean)

    
def main():
    app = QApplication(sys.argv)
    mywindow = Window(app)
    mywindow.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()