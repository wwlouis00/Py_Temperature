# SulinkIOT_temperature

## 溫度熱保護器

## EGGI主機
### Step1: 輸入 ID以及eGGi IP
    1. 自動產生
    2. 成功連線後會將"factory.csv" 放入該資料夾底下
### Step2: 進行"連接"
### Step3: 顯示曲線圖
    1. 成功連線後會將"merged_image.png" 放入/EGGI_COM/roi/
    2. 成功連線後會將"factory.csv" 放入該資料夾底下
### Step4: 進行資料儲存
    1. 第一次資料儲存會在/EGGI_COM/excel/資料夾底下並儲存檔名為今天日期
    2. 當天所有儲存結果都會累積在檔名為今天日期的excel裡面

- UI的變數名稱請參考ui/Sulink_Temperature_Final_0608.ui的功能參數
